<?php

/** 
 * Calcula el índice de masa corporal.
 * 
 * @param float $peso
 * @param int $altura La altura en centimetros
 * @return float
 */
function calcularIndiceMasaCorporal(float $peso, int $altura): float
{

    $alturaMetros = round($altura / 100, 2);

    $imc = $peso / pow($alturaMetros,2);

    return round($imc, 2);
    
}

$imc = calcularIndiceMasaCorporal(68, 165);

echo $imc."\n";

echo "<h1>HOLA MUNDO</h1>";