<html>
    <body>
    <?php

        $prestamos = [
            [
                'nombre' => 'JUAN',
                'edad' => 23,
                'monto' => 535454.45,
                'pagado' => true,
            ],
            [
                'nombre' => 'PEDRO',
                'edad' => 37,
                'monto' => 4354.45,
                'pagado' => false,
            ],
            [
                'nombre' => 'Isaac',
                'edad' => 45,
                'monto' => 546,
                'pagado' => false,
            ],
            [
                'nombre' => 'LIAM',
                'edad' => 17,
                'monto' => 3456.45,
                'pagado' => true,
            ],
            [
                'nombre' => 'Toño',
                'edad' => 27,
                'monto' => 1567,
                'pagado' => false,
            ],
        ];

        $totalPagado = 0;
        $totalPendiente = 0;

        foreach($prestamos as $indice => $prestamo)
        {
            
            if($prestamo['pagado'] === true)
            {
                $totalPagado = $totalPagado + $prestamo['monto'];
            } else {
                $totalPendiente = $totalPendiente + $prestamo['monto'];
            }


        }



        ?>
        <h1>Lista de prestamos</h1>
        <button type="button">Clickeame</button>
        <br/>
        <table>
            <thead>
                <tr>
                    <th>Consecutivo</th>
                    <th>Nombre</th>
                    <th>Edad</th>
                    <th>Monto</th>
                    <th>Pagado</th>
                </tr>
                
            </thead>
            <tbody>
                <?php foreach($prestamos as $indice => $prestamo):?>
                    <tr>
                        <td><?php echo $indice + 1 ?></td>
                        <td><?php echo mb_strtoupper($prestamo['nombre']) ; ?></td>
                        <td><?php echo $prestamo['edad']; ?></td>
                        <td><?php echo number_format($prestamo['monto'], 2); ?></td>
                        <td>
                            <?php if($prestamo['pagado'] === true): ?>
                                SI
                            <?php else: ?>
                                NO
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

        <p><b>Total Pagado:</b> <?php echo number_format($totalPagado,2); ?></p>
        <p><b>Total Pendiente:</b> <?php echo number_format($totalPendiente, 2); ?></p>

        <script type="text/javascript">
            //alert("hola mundo");
        </script>

    </body>
</html>

