<?php


/*
 Atributos o propiedades
 Metodos
 Constructor

 GIT
 git init iniciar el proyecto
 git status Checar el estado de los archivos
 git add .   Mandar al staging los cambios
 git commit -m "Mensaje"   Mandar al repositorio los cambios 
 git log con opciones --oneline  Ver el historial de los cambios
 git checkout commiti_d  Cambiarse a un commit o rama para ver los archivos en ese momento
 git branch lista las ramas


 */

class Persona 
{
    protected string $nombre;

    public int $edad;

    private string $rfc;

    public function __construct(?string $nombre = "DEFAULT")
    {
        echo "Soy el constructor de persona \n";
        $this->rfc = "XXXXXXXX";
        $this->nombre = $nombre;
    }

    public function __toString()
    {
        return $this->nombre;
    }


    public function setNombre(string $nombre)
    {
        $this->nombre = $nombre;
    }

    public function getNombre(): string
    {
        return $this->nombre;
    }

    public function setRfc(string $rfc): void
    {
        $this->rfc = $rfc;
    }

    public function getRfc(): string
    {
        return $this->rfc;
    }
}

class Empleado extends Persona
{
    private string $area;

    private float $sueldo;

    private $titulo;

    public function __construct()
    {
        $this->titulo = "C.";
        parent::__construct();
    }

    public function getNombre(): string
    {
        return $this->titulo." ".$this->nombre;
    }

    public function getArea(): string
    {
        return $this->area;
    }
}

$empleado1 = new Empleado();
$empleado1->setNombre("PEDRO");

echo $empleado1->getNombre();

//Es un objeto
// $persona1 = new Persona();
// $persona1->edad = 28;
// $persona1->setNombre("JUAN");

// echo $persona1->edad."<br/>";
// echo $persona1->getNombre()."\n";
// echo $persona1->getRfc()."\n";

// $persona2 = new Persona("ISAAC");
// echo $persona2->getNombre()."\n";

// echo $persona2;


